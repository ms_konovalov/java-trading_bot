package ms.konovalov.tradingbot.app;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.TestKit;
import akka.testkit.TestProbe;
import ms.konovalov.tradingbot.app.model.Protocol.*;
import ms.konovalov.tradingbot.app.model.Quote;
import ms.konovalov.tradingbot.app.model.Subscribe;
import org.junit.AfterClass;
import org.junit.Test;
import scala.concurrent.duration.Duration;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.*;

public class WSActorTest {

    private static ActorSystem system = ActorSystem.create();
    private final TraditionalWebSocketClient mock = mock(TraditionalWebSocketClient.class);
    private final WebSocketJsonParser parserMock = mock(WebSocketJsonParser.class);
    private final TestProbe probe = new TestProbe(system);
    private final ActorRef actor = system.actorOf(Props.create(WSActor.class, mock, probe.ref(), parserMock));

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system, Duration.create(10, TimeUnit.SECONDS), true);
        system = null;
    }

    @Test
    public void testConnect() {
        actor.tell(new ConnectCommand(), probe.ref());
        verify(mock, timeout(1000)).connect(anyObject(), anyObject(), anyObject());
    }

    @Test
    public void testConnectFailed() {
        ConnectFailedResult msg = new ConnectFailedResult();
        actor.tell(msg, probe.ref());
        probe.expectMsg(msg);
    }

    @Test
    public void testConnectSuccess() {
        ConnectedResult msg = new ConnectedResult();
        actor.tell(msg, probe.ref());
        probe.expectMsg(msg);
    }

    @Test
    public void testDisconnect() throws IOException {
        actor.tell(new DisconnectCommand(), probe.ref());
        verify(mock, timeout(1000)).disconnect();
    }

    @Test
    public void testSubscribe() throws IOException {
        String message = "message";
        when(parserMock.toJson(anyObject())).thenReturn(message);
        actor.tell(new SubscribeCommand(new Subscribe(new String[]{}, null)), probe.ref());
        verify(mock, timeout(1000)).sendMessage(message);
    }

    @Test
    public void testQuote() {
        QuoteMessageResult msg = new QuoteMessageResult(new Quote("", BigDecimal.ONE));
        actor.tell(msg, probe.ref());
        probe.expectMsg(msg);
    }


    @Test
    public void testxxx() {
        actor.tell("xxx", probe.ref());
        probe.expectNoMsg();
        verifyNoMoreInteractions(mock, parserMock);
    }
}