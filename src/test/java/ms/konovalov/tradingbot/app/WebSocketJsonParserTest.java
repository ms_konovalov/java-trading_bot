package ms.konovalov.tradingbot.app;

import ms.konovalov.tradingbot.app.model.Protocol;
import ms.konovalov.tradingbot.app.model.Protocol.QuoteMessageResult;
import ms.konovalov.tradingbot.app.model.Protocol.UnknownMessageResult;
import org.junit.Assert;
import org.junit.Test;

public class WebSocketJsonParserTest {

    private WebSocketJsonParser parser = new WebSocketJsonParser();

    @Test
    public void testConnect() {
        Object o = parser.parseMessage("{\n" +
                "  \"t\": \"connect.connected\",\n" +
                "  \"body\": {\n" +
                "    \"a\": 123\n" +
                "  }\n" +
                "}");
        Assert.assertTrue(o instanceof Protocol.ConnectedResult);
    }

    @Test
    public void testConnectFailed() {
        Object o = parser.parseMessage("{\n" +
                "\"t\": \"connect.failed\",\n" +
                "\"body\": {\n" +
                "\"developerMessage\": \"Missing JWT Access Token in request\",\n" +
                "\"errorCode\": \"RTF_002\"\n" +
                "}\n" +
                "}");
        Assert.assertTrue(o instanceof Protocol.ConnectFailedResult);
    }

    @Test
    public void testQuote() {
        Object o = parser.parseMessage("{\n" +
                "  \"t\": \"trading.quote\",\n" +
                "  \"body\": {\n" +
                "    \"securityId\": \"{productId}\",\n" +
                "    \"currentPrice\": \"10692.3\"\n" +
                "  }\n" +
                "}");
        Assert.assertTrue(o instanceof QuoteMessageResult);
    }

    @Test
    public void testxxx() {
        Object o = parser.parseMessage("blabla");
        Assert.assertTrue(o instanceof UnknownMessageResult);
        o = parser.parseMessage("{}");
        Assert.assertTrue(o instanceof UnknownMessageResult);
    }

}