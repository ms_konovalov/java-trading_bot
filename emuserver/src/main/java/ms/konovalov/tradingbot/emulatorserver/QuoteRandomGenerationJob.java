package ms.konovalov.tradingbot.emulatorserver;

import com.google.gson.Gson;
import ms.konovalov.tradingbot.emulatorserver.model.TradingQuote;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Job takes quote price from {@link RandomPriceGenerator} and sends it to WebSocket
 */
class QuoteRandomGenerationJob {

    private final WebSocketSession session;
    private final String productId;
    private final AtomicBoolean stopped = new AtomicBoolean(false);
    private final RandomPriceGenerator generator;

    QuoteRandomGenerationJob(WebSocketSession session, String productId, BigDecimal startPrice, BigDecimal maxPrice, BigDecimal step) {
        this.session = session;
        this.productId = productId;
        this.generator = new RandomPriceGenerator(startPrice, maxPrice, step);
    }

    CompletableFuture<Void> start() {
        return CompletableFuture.runAsync(() -> {
            while (true) {
                if (stopped.get()) {
                    return;
                }
                if (!session.isOpen()) {
                    return;
                }
                try {
                    String payload = new Gson().toJson(new TradingQuote(productId, generator.getNext()));
                    System.out.println("sending message " + payload);
                    session.sendMessage(new TextMessage(payload));
                } catch (IOException ignored) {
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
            }
        });
    }

    void stop() {
        stopped.compareAndSet(false, true);
    }
}
