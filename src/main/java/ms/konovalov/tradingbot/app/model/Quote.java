package ms.konovalov.tradingbot.app.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@RequiredArgsConstructor
@ToString
public class Quote {

    private final String securityId;
    private final BigDecimal currentPrice;
}
