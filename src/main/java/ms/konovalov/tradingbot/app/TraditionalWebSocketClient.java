package ms.konovalov.tradingbot.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator;
import org.springframework.web.socket.handler.LoggingWebSocketHandlerDecorator;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

/**
 * Client for WebSocket connection
 */
@Component
public class TraditionalWebSocketClient {

    private static final String LANG = "nl-NL,en;q=0.8";
    private static final String AUTH = "Bearer " +
            "eyJhbGciOiJIUzI1NiJ9.eyJyZWZyZXNoYWJsZSI6ZmFsc2UsInN1YiI6IjJlMDcxNjllLTN" +
            "lMTMtNGFjOS04M2JkLTFiN2I5ZTYwZGM2OSIsImF1ZCI6ImJldGEuZ2V0YnV4LmNvbSIsInN" +
            "jcCI6WyJhcHA6bG9naW4iLCJydGY6bG9naW4iXSwibmJmIjoxNDczOTQyMzk0LCJleHAiOjE" +
            "1MDU0NzgzOTQsImlhdCI6MTQ3Mzk0MjM5NCwianRpIjoiMzgyNTVlNmMtMTU0MC00OGJkLWI" +
            "xMjUtZWVkMmYyZjRjZDdlIiwiY2lkIjoiODQ3MzYyMjkzNCJ9.1r55eMkL7s2kBMio9KqHqS" +
            "7NYHdBMT7LAJOFc2U08Lg";
    private static final int SEND_TIME_LIMIT = 10 * 1000;
    private static final int SEND_BUFFER_SIZE_LIMIT = 512 * 1024;

    private final String wsUrl;
    private final AtomicReference<WebSocketSession> webSocketSession = new AtomicReference<>();

    @Autowired
    public TraditionalWebSocketClient(@Qualifier("wsUrl") String wsUrl) {
        this.wsUrl = wsUrl;
    }

    /**
     * Establish the connection to the server
     *
     * @param onConnect        functionality to invoke if connect successful
     * @param onConnectFailure functionality to invoke if connect was unsuccessful
     * @param onMessage        functionality to invoke on eacj message received from WebSocket
     */
    void connect(Runnable onConnect, FailureCallback onConnectFailure, Consumer<String> onMessage) {

        WebSocketHandler handler = new LoggingWebSocketHandlerDecorator(handleMessage(onMessage));
        new StandardWebSocketClient()
                .doHandshake(handler, fillHeaders(), URI.create(getUrl()))
                .addCallback(session -> {
                            if (session != null) {
                                set(session);
                                onConnect.run();
                            }
                        },
                        onConnectFailure);
    }

    private String getUrl() {
        if (wsUrl.startsWith("http")) {
            return "ws" + wsUrl.substring(4);
        }
        return wsUrl;
    }

    /*
     * After connection established we need to store session and wrap it to make thread-safe
     */
    private void set(WebSocketSession session) {
        webSocketSession.set(new ConcurrentWebSocketSessionDecorator(session, SEND_TIME_LIMIT, SEND_BUFFER_SIZE_LIMIT));
    }

    private WebSocketHttpHeaders fillHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.ACCEPT_LANGUAGE, LANG);
        headers.add(HttpHeaders.AUTHORIZATION, AUTH);
        return new WebSocketHttpHeaders(headers);
    }

    /*
     * Treat all messages as text
     */
    private TextWebSocketHandler handleMessage(Consumer<String> onMessage) {
        return new TextWebSocketHandler() {

            @Override
            protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
                onMessage.accept(message.getPayload());
            }
        };
    }

    /**
     * Send text message into WebSocket
     *
     * @param payload string payload
     * @throws IOException if socket is unavailable
     */
    void sendMessage(String payload) throws IOException {
        WebSocketSession session = webSocketSession.get();
        if (session != null) {
            session.sendMessage(new TextMessage(payload));
        }
    }

    /**
     * Disconnect from WebSocket
     *
     * @throws IOException if socket is unavailable
     */
    void disconnect() throws IOException {
        WebSocketSession session = webSocketSession.getAndSet(null);
        if (session != null) {
            session.close();
        }
    }
}
