package ms.konovalov.tradingbot.emulatorserver.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.time.Clock;
import java.util.UUID;

@Getter
public class ConnectResult extends RootEntity {

    public static final String SUCCESS = "connect.connected";
    public static final String FAILURE = "connect.failed";

    private final String id;
    private final int v = 1;

    @Getter
    private static class OkBody implements RootEntity.Body {
        private final String sessionId;
        private final long time = Clock.systemUTC().millis();
        private final Pop pop;
        private final String clientVersion = "UNKNOWN";

        private OkBody(String sessionId, String clientId) {
            this.sessionId = sessionId;
            this.pop = new Pop(clientId, sessionId);
        }
    }

    @RequiredArgsConstructor
    private static class FailBody implements RootEntity.Body {
        private final String developerMessage;
        private final String errorCode;
    }

    @Getter
    @RequiredArgsConstructor
    private static class Pop {
        private final String clientId;
        private final String sessionId;
    }

    private ConnectResult(String result, RootEntity.Body body) {
        super(result, body);
        this.id = UUID.randomUUID().toString();
    }

    public static ConnectResult success(String clientId) {
        return new ConnectResult(SUCCESS, new OkBody(UUID.randomUUID().toString(), clientId));
    }

    public static ConnectResult failure(String errorCode, String errorMessage) {
        return new ConnectResult(FAILURE, new FailBody(errorMessage, errorCode));
    }

    public boolean isSuccess() {
        return SUCCESS.equals(t);
    }
}
