package ms.konovalov.tradingbot.emulatorserver.model;

import lombok.Data;

@Data
public class Product {
    private String securityId;
    private String symbol;
    private String displayName;
}