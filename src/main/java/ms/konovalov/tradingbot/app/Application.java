package ms.konovalov.tradingbot.app;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.util.Timeout;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import lombok.RequiredArgsConstructor;
import ms.konovalov.tradingbot.app.model.Protocol.InitMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static akka.pattern.PatternsCS.ask;
import static ms.konovalov.tradingbot.app.SpringExtension.SPRING_EXTENSION_PROVIDER;
import static scala.compat.java8.FutureConverters.toJava;

@SuppressWarnings("SpringAutowiredFieldsWarningInspection")
@SpringBootApplication
@Configuration
@ComponentScan(basePackages = {"ms.konovalov.tradingbot.app"})
public class Application {

    private static final Timeout TIMEOUT = Timeout.apply(1, TimeUnit.HOURS);
    private static final String WS_URL = "https://rtf.beta.getbux.com/subscriptions/me";
    private static final String API_URL = "https://api.beta.getbux.com";

    @Autowired private ConfigurableApplicationContext applicationContext;
    @Autowired private ConsoleInteraction console;
    @Autowired @Qualifier("stopper") private Runnable stopper;
    @Autowired private ActorSystem system;
    @Autowired @Qualifier("tradingActor") private ActorRef actor;
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired private ApplicationArguments applicationArguments;
    @Autowired private RunArgs arguments;

    @Bean(destroyMethod = "terminate")
    public ActorSystem actorSystem() {
        ActorSystem system = ActorSystem.create("system");
        SPRING_EXTENSION_PROVIDER.get(system).initialize(applicationContext);
        return system;
    }

    @Bean(name = "arguments")
    public RunArgs parseArgs() {
        RunArgs param = new RunArgs();
        JCommander jCommander = JCommander.newBuilder().addObject(param).build();
        jCommander.parse(applicationArguments.getSourceArgs());
        return param;
    }

    @Bean(name = "tradingActor")
    public ActorRef trading() {
        return system.actorOf(SPRING_EXTENSION_PROVIDER.get(system).props("TradingActor"), "tradingActor");
    }

    @Bean(name = "wsActor")
    public ActorRef ws() {
        return system.actorOf(SPRING_EXTENSION_PROVIDER.get(system).props("WSActor"), "wsActor");
    }

    @Bean(name = "bsActor")
    public ActorRef bs() {
        return system.actorOf(SPRING_EXTENSION_PROVIDER.get(system).props("BuySellActor"), "bsActor");
    }

    @Bean(name = "wsUrl")
    public String wsUrl() {
        return !StringUtils.isEmpty(arguments.wsUrl) ? arguments.wsUrl : WS_URL;
    }

    @Bean(name = "apiUrl")
    public String apiUrl() {
        return !StringUtils.isEmpty(arguments.apiUrl) ? arguments.apiUrl : API_URL;
    }

    @Bean
    public StartListener listen() {
        return new StartListener((ContextRefreshedEvent event) -> doWork());
    }

    @Bean(name= "stopper")
    public Runnable stopper() {
        return this::stop;
    }

    private void doWork() {
        InitMessage initMessage = console.consoleInteraction();
        ask(actor, initMessage, TIMEOUT).toCompletableFuture().thenAccept(o -> {
            console.consoleFinish(o);
            toJava(system.terminate()).toCompletableFuture().thenAccept(o2 ->
                    stopper.run());
        });
    }

    private void stop() {
        applicationContext.close();
    }

    @RequiredArgsConstructor
    private static class StartListener {

        private final Consumer<ContextRefreshedEvent> consumer;

        @EventListener
        public void handleContextRefresh(ContextRefreshedEvent event) {
            consumer.accept(event);
        }
    }

    public static void main(String[] args) throws IOException {
        SpringApplication app = new SpringApplication(Application.class);
        app.setWebApplicationType(WebApplicationType.NONE);
        app.run(args);
    }

    @Parameters(separators = "=")
    private static class RunArgs {
        @Parameter(names = {"-ws"}, description = "websocket URL")
        String wsUrl;
        @Parameter(names = {"-api"}, description = "api base url (scheme://host:port)")
        String apiUrl;
    }
}
