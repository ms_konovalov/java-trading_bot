package ms.konovalov.tradingbot.app.model;

public enum BuyDirection {
    BUY, SELL
}
