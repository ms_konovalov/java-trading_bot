package ms.konovalov.tradingbot.app;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ms.konovalov.tradingbot.app.model.Protocol.ConnectFailedResult;
import ms.konovalov.tradingbot.app.model.Protocol.ConnectedResult;
import ms.konovalov.tradingbot.app.model.Protocol.QuoteMessageResult;
import ms.konovalov.tradingbot.app.model.Protocol.UnknownMessageResult;
import ms.konovalov.tradingbot.app.model.Quote;
import ms.konovalov.tradingbot.app.model.Subscribe;
import org.springframework.stereotype.Component;

/**
 * Json Serializer/deserializer for messages over WebSocket
 */
@Component
public class WebSocketJsonParser {

    /**
     * Parse string Json message received from WebSocket
     *
     * @param message text payload
     * @return deserialized message. In case message is unknown it returns {@link UnknownMessageResult}
     */
    Object parseMessage(String message) {
        try {
            JsonObject rootEntity = new JsonParser().parse(message).getAsJsonObject();
            if (isConnectSuccess(rootEntity)) {
                return new ConnectedResult();
            }
            if (isConnectFailed(rootEntity)) {
                return new ConnectFailedResult();
            }
            if (isTradingQuote(rootEntity)) {
                return new QuoteMessageResult(toQuote(rootEntity));
            }
        } catch (Exception ignore) {
        }
        return new UnknownMessageResult();
    }

    private boolean isConnectSuccess(JsonObject entity) {
        return checkType(entity, "connect.connected");
    }

    private boolean isConnectFailed(JsonObject entity) {
        return checkType(entity, "connect.failed");
    }

    private boolean isTradingQuote(JsonObject entity) {
        return checkType(entity, "trading.quote");
    }

    private boolean checkType(JsonObject entity, String type) {
        JsonElement t = entity.get("t");
        return (t != null && type.equals(t.getAsString()));
    }

    private Quote toQuote(JsonObject entity) {
        JsonObject body = entity.get("body").getAsJsonObject();
        return new Quote(body.get("securityId").getAsString(), body.get("currentPrice").getAsBigDecimal());
    }

    /**
     * Serializes subscribe/unsubscribe message into Json
     *
     * @param subscribe entity
     * @return serialized json string
     */
    String toJson(Subscribe subscribe) {
        return new Gson().toJson(subscribe);
    }

}
