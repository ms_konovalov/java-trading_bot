package ms.konovalov.tradingbot.app;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import ms.konovalov.tradingbot.app.model.Protocol.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Actor responsible for WebSocket interaction
 */
@Component("WSActor")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class WSActor extends AbstractLoggingActor {

    private final TraditionalWebSocketClient wsClient;
    private final ActorRef mainActor;
    private final WebSocketJsonParser parser;

    @Autowired
    public WSActor(TraditionalWebSocketClient wsClient, @Qualifier("tradingActor") ActorRef mainActor, WebSocketJsonParser parser) {
        this.wsClient = wsClient;
        this.mainActor = mainActor;
        this.parser = parser;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ConnectCommand.class, message -> connect())

                .match(DisconnectCommand.class, message -> disconnect())

                .match(SubscribeCommand.class, this::subscribe)

                .match(ConnectFailedResult.class, this::connectFailed)

                .match(ConnectedResult.class, this::connectSuccess)

                .match(QuoteMessageResult.class, this::quote)

                .matchAny(o -> log().debug("received unknown message " + o.getClass()))
                .build();
    }

    private void quote(QuoteMessageResult message) {
        mainActor.tell(message, self());
    }

    private void connectSuccess(ConnectedResult message) {
        mainActor.tell(message, self());
    }

    private void connectFailed(ConnectFailedResult message) {
        mainActor.tell(message, self());
    }

    private void subscribe(SubscribeCommand message) throws IOException {
        wsClient.sendMessage(parser.toJson(message.getSubscribe()));
    }

    private void disconnect() throws IOException {
        wsClient.disconnect();
    }

    private void connect() {
        wsClient.connect(
                () -> {},
                ex -> self().tell(new ConnectFailedResult(), self()),
                m -> self().tell(parser.parseMessage(m), self())
        );
    }
}