package ms.konovalov.tradingbot.app;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.TestKit;
import akka.testkit.TestProbe;
import ms.konovalov.tradingbot.app.model.Protocol;
import ms.konovalov.tradingbot.app.model.Protocol.*;
import ms.konovalov.tradingbot.app.model.Quote;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

public class TradingActorTest {

    private static final Protocol.InitMessage INIT_MESSAGE = new Protocol.InitMessage("id", BigDecimal.ONE,
            BigDecimal.ONE, BigDecimal.valueOf(2), BigDecimal.TEN);
    private static final FiniteDuration SEC = FiniteDuration.apply(300, TimeUnit.MILLISECONDS);
    private static ActorSystem system = ActorSystem.create();
    private final TestProbe probe = new TestProbe(system);
    private final TestProbe wsProbe = new TestProbe(system);
    private final TestProbe bsProbe = new TestProbe(system);
    @SuppressWarnings("RedundantArrayCreation")
    private final ActorRef actor = system.actorOf(Props.create(TradingActor.class, new Object[]{bsProbe.ref(), wsProbe.ref()}));

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system, Duration.create(10, TimeUnit.SECONDS), true);
        system = null;
    }

    @Test
    public void testInit() throws BuySellClient.BuySellException {
        initActor();
    }

    @Test
    public void testSubscribe() throws BuySellClient.BuySellException {
        initActor();
        actor.tell(new ConnectedResult(), wsProbe.ref());
        wsProbe.expectMsgClass(SubscribeCommand.class);
    }

    @Test
    public void testConnectFailed() throws BuySellClient.BuySellException {
        initActor();
        actor.tell(new ConnectFailedResult(), wsProbe.ref());
        probe.expectMsgClass(Finish.class);
        Assert.assertFalse(((Finish) probe.lastMessage().msg()).isSuccess());
    }

    @Test
    public void testQuote() throws BuySellClient.BuySellException {
        //should ignore
        initActor();
        Quote quote = new Quote("xxx", BigDecimal.ONE);
        actor.tell(new QuoteMessageResult(quote), wsProbe.ref());
        ignore();

        //should ignore
        quote = new Quote(INIT_MESSAGE.getProductId(), INIT_MESSAGE.getBuyPrice().add(BigDecimal.ONE));
        actor.tell(new QuoteMessageResult(quote), wsProbe.ref());
        ignore();

        shouldBuy();

        //should ignore
        quote = new Quote(INIT_MESSAGE.getProductId(), INIT_MESSAGE.getBuyPrice().subtract(BigDecimal.ONE));
        actor.tell(new QuoteMessageResult(quote), wsProbe.ref());
        ignore();

        actor.tell(new BuyResult(true, "qwerty"), bsProbe.ref());
        ignore();

        shouldSell();
    }

    @Test
    public void testBought() throws BuySellClient.BuySellException {
        initActor();
        shouldBuy();

        actor.tell(new BuyResult(true, "qwerty"), bsProbe.ref());
        ignore();
    }

    @Test
    public void testBuyFailed() throws BuySellClient.BuySellException {
        initActor();
        shouldBuy();

        actor.tell(new BuyResult(false, null), bsProbe.ref());
        ignore();
    }

    @Test
    public void testSold() throws BuySellClient.BuySellException {
        initActor();
        shouldBuy();

        actor.tell(new BuyResult(true, "qwerty"), bsProbe.ref());
        ignore();

        shouldSell();

        actor.tell(new SellResult(true, BigDecimal.ZERO), bsProbe.ref());
        bsProbe.expectNoMsg(SEC);
        wsProbe.expectMsgClass(SubscribeCommand.class);
        wsProbe.expectMsgClass(DisconnectCommand.class);
        probe.expectMsgClass(Finish.class);
    }

    @Test
    public void testSellFailed() throws BuySellClient.BuySellException {
        initActor();
        shouldBuy();

        actor.tell(new BuyResult(true, "id"), bsProbe.ref());
        ignore();

        shouldSell();

        actor.tell(new SellResult(false, null), bsProbe.ref());
        ignore();
    }

    @Test
    public void testxxx() throws BuySellClient.BuySellException {
        actor.tell("xxx", bsProbe.ref());
        ignore();
    }

    private void ignore() {
        bsProbe.expectNoMsg(SEC);
        wsProbe.expectNoMsg(SEC);
    }

    private void initActor() {
        actor.tell(INIT_MESSAGE, probe.ref());
        wsProbe.expectMsgClass(ConnectCommand.class);
    }

    private void shouldBuy() {
        Quote quote = new Quote(INIT_MESSAGE.getProductId(), INIT_MESSAGE.getBuyPrice().subtract(BigDecimal.ONE));
        actor.tell(new QuoteMessageResult(quote), wsProbe.ref());
        bsProbe.expectMsgClass(BuyCommand.class);
        wsProbe.expectNoMsg(SEC);
    }

    private void shouldSell() {
        Quote quote = new Quote(INIT_MESSAGE.getProductId(), INIT_MESSAGE.getSellMinPrice().add(BigDecimal.ONE));
        actor.tell(new QuoteMessageResult(quote), wsProbe.ref());
        bsProbe.expectMsgClass(SellCommand.class);
        wsProbe.expectNoMsg(SEC);
    }
}