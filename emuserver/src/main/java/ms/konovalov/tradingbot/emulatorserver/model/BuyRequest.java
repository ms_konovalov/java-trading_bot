package ms.konovalov.tradingbot.emulatorserver.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class BuyRequest {

    private final String productId;
    private final Money investingAmount;
    private final int leverage = 1;
    private final BuyDirection direction = BuyDirection.BUY;

}
