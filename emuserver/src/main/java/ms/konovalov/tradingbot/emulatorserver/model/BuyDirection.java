package ms.konovalov.tradingbot.emulatorserver.model;

public enum BuyDirection {
    BUY, SELL
}
