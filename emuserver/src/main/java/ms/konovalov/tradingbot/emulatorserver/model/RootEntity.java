package ms.konovalov.tradingbot.emulatorserver.model;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class RootEntity {

    protected final String t;
    protected final Body body;

    interface Body {
    }
}
