package ms.konovalov.tradingbot.app;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.testkit.TestKit;
import akka.testkit.TestProbe;
import ms.konovalov.tradingbot.app.BuySellClient.BuySellException;
import ms.konovalov.tradingbot.app.model.Protocol.BuyCommand;
import ms.konovalov.tradingbot.app.model.Protocol.BuyResult;
import ms.konovalov.tradingbot.app.model.Protocol.SellCommand;
import ms.konovalov.tradingbot.app.model.Protocol.SellResult;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import scala.concurrent.duration.Duration;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class BuySellActorTest {

    private static ActorSystem system = ActorSystem.create();
    private final BuySellClient mock = mock(BuySellClient.class);
    private final TestProbe probe = new TestProbe(system);
    @SuppressWarnings("RedundantArrayCreation")
    private final ActorRef actor = system.actorOf(Props.create(BuySellActor.class, new Object[]{mock}));

    @AfterClass
    public static void teardown() {
        TestKit.shutdownActorSystem(system, Duration.create(10, TimeUnit.SECONDS), true);
        system = null;
    }

    @Test
    public void testBuy() throws BuySellException {
        when(mock.buy(anyString(), anyObject())).thenReturn("id");
        actor.tell(new BuyCommand("123", BigDecimal.TEN), probe.ref());
        verify(mock, timeout(1000)).buy(anyString(), anyObject());
        probe.expectMsgClass(BuyResult.class);
        Assert.assertEquals("id", ((BuyResult) probe.lastMessage().msg()).getId());
    }

    @Test
    public void testFailBuy() throws BuySellException {
        when(mock.buy(anyString(), anyObject())).thenThrow(new BuySellException(""));
        actor.tell(new BuyCommand("123", BigDecimal.TEN), probe.ref());
        verify(mock, timeout(1000)).buy(anyString(), anyObject());
        probe.expectMsgClass(BuyResult.class);
        Assert.assertFalse(((BuyResult) probe.lastMessage().msg()).isOk());
    }

    @Test
    public void testSell() throws BuySellException {
        when(mock.sell(anyString())).thenReturn(BigDecimal.TEN);
        actor.tell(new SellCommand("123"), probe.ref());
        verify(mock, timeout(1000)).sell(anyString());
        probe.expectMsgClass(SellResult.class);
        Assert.assertEquals("10", ((SellResult) probe.lastMessage().msg()).getProfit().toString());
    }

    @Test
    public void testSellFail() throws BuySellException {
        when(mock.sell(anyString())).thenThrow(new BuySellException(""));
        actor.tell(new SellCommand("123"), probe.ref());
        verify(mock, timeout(1000)).sell(anyString());
        probe.expectMsgClass(SellResult.class);
        Assert.assertFalse(((SellResult) probe.lastMessage().msg()).isOk());
    }

    @Test
    public void testTrash() throws BuySellException {
        actor.tell("blablabla", probe.ref());
        verify(mock, never()).sell(anyString());
        verify(mock, never()).buy(anyString(), anyObject());
        probe.expectNoMsg();
    }

}