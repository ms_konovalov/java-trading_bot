package ms.konovalov.tradingbot.app;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import ms.konovalov.tradingbot.app.model.Protocol.*;
import ms.konovalov.tradingbot.app.model.Quote;
import ms.konovalov.tradingbot.app.model.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static ms.konovalov.tradingbot.app.TradingActor.State.*;

/**
 * Actor implementing trading algorithm
 */
@Component("TradingActor")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class TradingActor extends AbstractLoggingActor {

    private final ActorRef bsActor;
    private final ActorRef wsActor;
    /* Original sender of InitMessage to send back result */
    private ActorRef initSender;
    /* Init message with trading params */
    private InitMessage init;
    /* State of actor - representing FSM */
    private State state = INITIAL;
    /* identifier of bought position */
    private String positionId;

    @Autowired
    public TradingActor(@Qualifier("bsActor") ActorRef bsActor, @Qualifier("wsActor") ActorRef wsActor) {
        this.bsActor = bsActor;
        this.wsActor = wsActor;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(InitMessage.class, this::init)

                .match(ConnectedResult.class, message -> subscribe())

                .match(ConnectFailedResult.class, message -> connectFailed())

                .match(QuoteMessageResult.class, this::processQuote)

                .match(BuyResult.class, this::bought)

                .match(SellResult.class, this::sold)

                .matchAny(o -> log().info("received unknown message"))
                .build();
    }

    private void init(InitMessage message) {
        this.init = message;
        this.initSender = getSender();
        wsActor.tell(new ConnectCommand(), self());
    }

    private void subscribe() {
        wsActor.tell(new SubscribeCommand(Subscribe.subscribe(init.getProductId())), self());
    }

    /*
     * Process Quotes signals.
     * We skip all unrelated signals
     * If in INITIAL state, we buy. If in BOUGHT state, we sell
     */
    private void processQuote(QuoteMessageResult message) {
        Quote quote = message.getQuote();
        if (quote == null || !init.getProductId().equals(quote.getSecurityId())) {
            return;
        }
        log().debug(quote.toString());
        if (state == INITIAL && shouldBuy(quote)) {
            state = PENDING_BUY;
            bsActor.tell(new BuyCommand(init.getProductId(), init.getAmount()), self());
            return;
        }
        if (state == BOUGHT && shouldSell(quote)) {
            state = PENDING_SELL;
            bsActor.tell(new SellCommand(positionId), self());
        }
    }

    private boolean shouldBuy(Quote quote) {
        return quote.getCurrentPrice().compareTo(init.getBuyPrice()) <= 0;
    }

    private boolean shouldSell(Quote quote) {
        return quote.getCurrentPrice().compareTo(init.getSellMaxPrice()) <= 0
                && quote.getCurrentPrice().compareTo(init.getSellMinPrice()) >= 0;
    }

    /*
     * If sell result was unsuccessful - go back to BOUGHT state.
     * Otherwise disconnect from WebSocket and return result
     */
    private void sold(SellResult message) {
        if (message.isOk()) {
            state = SOLD;
            wsActor.tell(unsubscribe(), self());
            wsActor.tell(new DisconnectCommand(), self());
            initSender.tell(new Finish(message.getProfit(), true), self());
        } else {
            state = BOUGHT;
        }
    }

    private void connectFailed() {
        initSender.tell(new Finish(null, false), self());
    }

    private SubscribeCommand unsubscribe() {
        return new SubscribeCommand(Subscribe.unsubscribe(init.getProductId()));
    }


    /*
     * If buy result was unsuccessful - go to INITIAL state. Otherwise - to BOUGHT
     */
    private void bought(BuyResult message) {
        if (message.isOk()) {
            state = BOUGHT;
            positionId = message.getId();
        } else {
            state = INITIAL;
        }
    }

    /**
     * Actor states. It is created in INITIAL state.
     * When we send signal to buy actor moves to PENDING_BUY
     * When we receive buy acknowledge - BOUGHT
     * When we send signal to sell - PENDING_SELL
     * When we receive sell acknowledge - SOLD
     */
    enum State {
        INITIAL, PENDING_BUY, BOUGHT, PENDING_SELL, SOLD
    }

}