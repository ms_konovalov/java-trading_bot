package ms.konovalov.tradingbot.app;

import ms.konovalov.tradingbot.app.model.BuyRequest;
import ms.konovalov.tradingbot.app.model.BuyResponse;
import ms.konovalov.tradingbot.app.model.Money;
import ms.konovalov.tradingbot.app.model.SellResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

/**
 * Client for REST buy/sell calls
 */
@Component
public class BuySellClient {

    private static final String BUY_URL_SUFFIX = "/core/16/users/me/trades";
    private static final String SELL_URL_SUFFIX = "/core/16/users/me/portfolio/positions/{positionId}";
    private RestTemplate client = new RestTemplate();
    private final String baseUrl;
    private static final MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>() {{
        add("Authorization", "Bearer eyJhbGciOiJIUzI1NiJ9.eyJyZWZyZXNoYWJsZSI6ZmFsc2UsInN1YiI6IjJlMDcxNjllLTNlMTMtNGFjOS04M2JkLTFiN2I5ZTYwZGM2OSIsImF1ZCI6ImJldGEuZ2V0YnV4LmNvbSIsInNjcCI6WyJhcHA6bG9naW4iLCJydGY6bG9naW4iXSwibmJmIjoxNDczOTQyMzk0LCJleHAiOjE1MDU0NzgzOTQsImlhdCI6MTQ3Mzk0MjM5NCwianRpIjoiMzgyNTVlNmMtMTU0MC00OGJkLWIxMjUtZWVkMmYyZjRjZDdlIiwiY2lkIjoiODQ3MzYyMjkzNCJ9.1r55eMkL7s2kBMio9KqHqS7NYHdBMT7LAJOFc2U08Lg");
        add("Accept-Language", "nl-NL,en;q=0.8");
        add("Content-Type", "application/json");
        add("Accept", "application/json");
    }};

    @Autowired
    BuySellClient(@Qualifier("apiUrl") String baseUrl) {
        this.baseUrl = baseUrl;
    }

    String buy(String productId, BigDecimal amount) throws BuySellException {
        HttpEntity<BuyRequest> request = new HttpEntity<>(new BuyRequest(productId, new Money(amount)), headers);
        try {
            ResponseEntity<BuyResponse> response = client.postForEntity(buyUrl(), request, BuyResponse.class);
            BuyResponse body = response.getBody();
            if (body != null) {
                return body.getPositionId();
            }
        } catch (Exception ex) {
            throw new BuySellException(ex);
        }
        throw new BuySellException("Body is absent");
    }

    BigDecimal sell(String positionId) throws BuySellException {
        try {
            HttpEntity<BuyRequest> request = new HttpEntity<>(headers);
            ResponseEntity<SellResponse> response = client.exchange(sellUrl(), HttpMethod.DELETE, request, SellResponse.class, positionId);
            SellResponse body = response.getBody();
            if (body != null && body.getProfitAndLoss() != null) {
                return new BigDecimal(body.getProfitAndLoss().getAmount());
            }
        } catch (Exception e) {
            throw new BuySellException(e);
        }
        throw new BuySellException("Body is absent");
    }

    private String buyUrl() {
        return baseUrl + BUY_URL_SUFFIX;
    }

    private String sellUrl() {
        return baseUrl + SELL_URL_SUFFIX;
    }

    static class BuySellException extends Exception {

        BuySellException(String message) {
            super(message);
        }

        BuySellException(Exception e) {
            super(e);
        }
    }
}
