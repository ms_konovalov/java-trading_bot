package ms.konovalov.tradingbot.app;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import ms.konovalov.tradingbot.app.BuySellClient.BuySellException;
import ms.konovalov.tradingbot.app.model.Protocol.BuyCommand;
import ms.konovalov.tradingbot.app.model.Protocol.BuyResult;
import ms.konovalov.tradingbot.app.model.Protocol.SellCommand;
import ms.konovalov.tradingbot.app.model.Protocol.SellResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.concurrent.CompletableFuture;

/**
 * Actor responsible for buy/sell calls
 */
@Component("BuySellActor")
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BuySellActor extends AbstractLoggingActor {

    private final BuySellClient client;

    @Autowired
    public BuySellActor(BuySellClient client) {
        this.client = client;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(BuyCommand.class, message -> buy(message, getSender()))

                .match(SellCommand.class, message -> sell(message, getSender()))

                .matchAny(o -> log().info("received unknown message"))
                .build();
    }

    /*
     * Make async buy
     */
    private void buy(BuyCommand message, ActorRef sender) {
        CompletableFuture
                .supplyAsync(() -> buyBlocking(message))
                .thenAccept(result -> sender.tell(result, self()));
    }

    /*
     * Buy with clocking, in case of error return false result
     */
    private BuyResult buyBlocking(BuyCommand message) {
        try {
            String id = client.buy(message.getProductId(), message.getAmount());
            return new BuyResult(true, id);
        } catch (BuySellException e) {
            return new BuyResult(false, null);
        }
    }

    /*
     * Make async sell
     */
    private void sell(SellCommand message, ActorRef sender) {
        CompletableFuture
                .supplyAsync(() -> sellBlocking(message))
                .thenAccept(result -> sender.tell(result, self()));
    }

    /*
     * Sell with clocking, in case of error return false result
     */
    private SellResult sellBlocking(SellCommand message) {
        try {
            BigDecimal sell = client.sell(message.getId());
            return new SellResult(true, sell);
        } catch (BuySellException e) {
            return new SellResult(false, null);
        }
    }
}