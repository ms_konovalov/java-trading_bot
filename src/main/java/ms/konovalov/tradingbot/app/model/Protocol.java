package ms.konovalov.tradingbot.app.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

public interface Protocol {

    class ConnectCommand {}

    class DisconnectCommand {}

    class ConnectFailedResult {}

    class ConnectedResult {}

    class UnknownMessageResult {}

    @Getter
    @RequiredArgsConstructor
    class QuoteMessageResult {
        private final Quote quote;
    }

    @Getter
    @RequiredArgsConstructor
    class SubscribeCommand {
        private final Subscribe subscribe;
    }

    @Getter
    @RequiredArgsConstructor
    class InitMessage {
        private final String productId;
        private final BigDecimal amount;
        private final BigDecimal buyPrice;
        private final BigDecimal sellMinPrice;
        private final BigDecimal sellMaxPrice;
    }

    @Getter
    @RequiredArgsConstructor
    class BuyCommand {
        private final String productId;
        private final BigDecimal amount;
    }

    @Getter
    @RequiredArgsConstructor
    class BuyResult {
        private final boolean ok;
        private final String id;
    }

    @Getter
    @RequiredArgsConstructor
    class SellCommand {
        private final String id;
    }

    @Getter
    @RequiredArgsConstructor
    class SellResult {
        private final boolean ok;
        private final BigDecimal profit;
    }

    @Getter
    @RequiredArgsConstructor
    class Finish {
        private final BigDecimal profit;
        private final boolean success;
    }
}
