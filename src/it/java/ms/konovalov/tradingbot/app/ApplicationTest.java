package ms.konovalov.tradingbot.app;

import ms.konovalov.tradingbot.app.model.Protocol.InitMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {Application.class, ApplicationTest.class})
@Configuration
public class ApplicationTest {

    private final String emuUrl = System.getProperty("emu.url") != null ? System.getProperty("emu.url") : "http://localhost:8080";
    private static final CountDownLatch latch = new CountDownLatch(1);

    @Bean(name = "wsUrl")
    public String wsUrl() {
        return emuUrl + "/subscriptions/me";
    }

    @Bean(name = "apiUrl")
    public String apiUrl() {
        return emuUrl;
    }

    @Bean
    @Primary
    public ConsoleInteraction ci() {
        return new ConsoleInteraction() {
            InitMessage consoleInteraction() {
                return new InitMessage("sp", BigDecimal.TEN, BigDecimal.valueOf(30), BigDecimal.valueOf(60), BigDecimal.valueOf(90));
            }
        };
    }

    @Bean(name = "stopper")
    @Primary
    public Runnable stopper() {
        return latch::countDown;
    }

    @Test
    public void testStartUp() throws InterruptedException {
        latch.await();
    }
}