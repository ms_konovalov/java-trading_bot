package ms.konovalov.tradingbot.app;

import akka.actor.*;
import org.springframework.context.ApplicationContext;

/**
 * Connector between Spring DI and akka actors
 */
public class SpringExtension extends AbstractExtensionId<SpringExtension.SpringExt> {

    public static final SpringExtension SPRING_EXTENSION_PROVIDER = new SpringExtension();

    @Override
    public SpringExt createExtension(ExtendedActorSystem system) {
        return new SpringExt();
    }

    public static class SpringExt implements Extension {

        private volatile ApplicationContext applicationContext;

        public void initialize(ApplicationContext applicationContext) {
            this.applicationContext = applicationContext;
        }

        public Props props(String actorBeanName) {
            return Props.create(SpringActorProducer.class, applicationContext, actorBeanName);
        }

    }

    private static class SpringActorProducer implements IndirectActorProducer {

        private ApplicationContext applicationContext;

        private String beanActorName;

        public SpringActorProducer(ApplicationContext applicationContext, String beanActorName) {
            this.applicationContext = applicationContext;
            this.beanActorName = beanActorName;
        }

        @Override
        public Actor produce() {
            return (Actor) applicationContext.getBean(beanActorName);
        }

        @SuppressWarnings("unchecked")
        @Override
        public Class<? extends Actor> actorClass() {
            return (Class<? extends Actor>) applicationContext.getType(beanActorName);
        }

    }
}
