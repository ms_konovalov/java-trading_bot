package ms.konovalov.tradingbot.emulatorserver;

import lombok.RequiredArgsConstructor;
import org.springframework.web.socket.WebSocketSession;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class manages subscription jobs
 */
@RequiredArgsConstructor
public class SubscriptionManager {

    private final BigDecimal startPrice;
    private final BigDecimal maxPrice;
    private final BigDecimal step;
    private final Map<String, QuoteRandomGenerationJob> jobs = new ConcurrentHashMap<>();

    /*
     * Create new job if not exist
     */
    void addSubscription(WebSocketSession session, String[] subscribe) {
        Arrays.stream(subscribe).distinct().forEach(i -> {
                    String productId = prepareProductId(i);
                    jobs.computeIfAbsent(session.getId() + productId, key -> {
                        System.out.println("subscribe" + key);
                        QuoteRandomGenerationJob job = new QuoteRandomGenerationJob(session, productId, startPrice, maxPrice, step);
                        job.start();
                        return job;
                    });
                }
        );
    }

    /*
     * Stop generation job
     */
    void removeSubscription(WebSocketSession session, String[] subscribe) {
        Arrays.stream(subscribe).forEach(productId -> {
                    QuoteRandomGenerationJob remove = jobs.remove(session.getId() + productId);
                    if (remove != null) {
                        remove.stop();
                    }
                }
        );
    }

    private String prepareProductId(String productId) {
        if (productId != null && productId.startsWith("trading.product.")) {
            return productId.substring(16);
        }
        return productId;
    }

}
