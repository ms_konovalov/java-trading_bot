package ms.konovalov.tradingbot.emulatorserver;

import com.google.gson.*;
import ms.konovalov.tradingbot.emulatorserver.model.ConnectResult;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import org.springframework.web.socket.handler.LoggingWebSocketHandlerDecorator;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Emulator for testing purposes
 * Emulates WebSocket behavior and REST endpoints
 */
@Configuration
@EnableWebSocket
@EnableWebMvc
@SpringBootApplication
public class TraditionalWebSocketServer implements WebSocketConfigurer {

    private static final BigDecimal startPrice = BigDecimal.valueOf(100);
    private static final BigDecimal maxPrice = BigDecimal.valueOf(300);
    private static final BigDecimal step = BigDecimal.valueOf(20);
    private static final SubscriptionManager manager = new SubscriptionManager(startPrice, maxPrice, step);

    public static void main(String[] args) throws IOException {
        SpringApplication.run(TraditionalWebSocketServer.class, args);
    }

    @Override
    public void registerWebSocketHandlers(@Nullable WebSocketHandlerRegistry registry) {
        if (registry != null) {
            /* endpoint for regular behavior */
            registry.addHandler(new LoggingWebSocketHandlerDecorator(new WsHandler()), "/subscriptions/me");
            /* endpoint for connection failed */
            registry.addHandler(new LoggingWebSocketHandlerDecorator(new AbstractWebSocketHandler() {
                @Override
                public void afterConnectionEstablished(WebSocketSession session) throws Exception {
                    String payload = new Gson().toJson(ConnectResult.failure("ERR-CODE", "Unable to establish connection"));
                    session.sendMessage(new TextMessage(payload));
                }
            }), "/subscriptions/connectfailed");
        }
    }

    /**
     * Handle WebSocket messages
     * Saves subscription to {@link Map} and starts {@link QuoteRandomGenerationJob} for provided productId
     * When unsubscribe, stops generation job
     */
    public static class WsHandler extends TextWebSocketHandler {

        /*
         * Handle subscription messages from client
         */
        @Override
        public void handleTextMessage(final WebSocketSession session, TextMessage message) {
            String payload = message.getPayload();
            System.out.println(payload);
            JsonObject parse = new JsonParser().parse(payload).getAsJsonObject();
            JsonElement subscribeTo = parse.get("subscribeTo");
            if (subscribeTo != null) {
                manager.addSubscription(session, convert(subscribeTo.getAsJsonArray()));
            }
            JsonElement unsubscribeFrom = parse.get("unsubscribeFrom");
            if (unsubscribeFrom != null) {
                manager.removeSubscription(session, convert(unsubscribeFrom.getAsJsonArray()));
            }
        }

        /*
         * After connection send special confirmation message
         */
        @Override
        public void afterConnectionEstablished(WebSocketSession session) throws Exception {
            session.sendMessage(new TextMessage(new Gson().toJson(ConnectResult.success(session.getId()))));
        }

        private String[] convert(JsonArray array) {
            String[] strings = new String[array.size()];
            for (int i = 0; i < strings.length; i++) {
                strings[i] = array.get(i).getAsString();
            }
            return strings;
        }
    }
}
