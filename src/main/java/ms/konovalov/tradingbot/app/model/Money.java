package ms.konovalov.tradingbot.app.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Money {

    private String currency = "BUX";
    private int decimals = 2;
    private String amount;

    public Money(){
    }

    public Money(BigDecimal amount) {
        this.amount = amount.setScale(2, BigDecimal.ROUND_CEILING).toPlainString();
    }
}
