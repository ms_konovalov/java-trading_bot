package ms.konovalov.tradingbot.emulatorserver;

import ms.konovalov.tradingbot.emulatorserver.model.*;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.Clock;
import java.util.UUID;

/**
 * Mock REST controller that returns constant data
 */
@RestController
@RequestMapping("/core/16/users/me")
class BuySellController {

    @PostMapping(path = "/trades", consumes = "application/json", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public BuyResponse buy(BuyRequest buy) {
        BuyResponse response = new BuyResponse();
        response.setId(UUID.randomUUID().toString());
        response.setPositionId(UUID.randomUUID().toString());
        response.setDateCreated(Clock.systemUTC().millis());
        response.setDirection(buy.getDirection());
        response.setLeverage(buy.getLeverage());
        response.setPrice(new Money(BigDecimal.valueOf(100)));
        Product product = new Product();
        product.setDisplayName("product");
        product.setSecurityId(buy.getProductId());
        product.setSymbol("AAA");
        response.setProduct(product);
        response.setType("OPEN");
        response.setInvestingAmount(buy.getInvestingAmount());
        return response;
    }

    @DeleteMapping(path = "/portfolio/positions/{positionId}", consumes = "application/json", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public SellResponse sell(@PathVariable String positionId) {
        SellResponse response = new SellResponse();
        response.setId(UUID.randomUUID().toString());
        response.setPositionId(positionId);
        response.setDateCreated(Clock.systemUTC().millis());
        response.setType("CLOSE");
        response.setLeverage(1);
        Product product = new Product();
        product.setDisplayName("product");
        product.setSecurityId("productId");
        product.setSymbol("AAA");
        response.setProduct(product);
        response.setProfitAndLoss(new Money(BigDecimal.valueOf(1275.47)));
        response.setInvestingAmount(new Money(BigDecimal.valueOf(10)));
        response.setPrice(new Money(BigDecimal.valueOf(40.22)));
        response.setDirection(BuyDirection.SELL);
        return response;
    }
}
