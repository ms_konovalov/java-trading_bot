package ms.konovalov.tradingbot.emulatorserver;

import java.math.BigDecimal;

/**
 * Generates price between maxPrice and 0 with provided step
 */
class RandomPriceGenerator {

    private BigDecimal price;
    private final BigDecimal maxPrice;
    private final BigDecimal step;
    private int direction = -1;

    RandomPriceGenerator(BigDecimal startPrice, BigDecimal maxPrice, BigDecimal step) {
        this.price = startPrice;
        this.step = step;
        this.maxPrice = maxPrice.subtract(step);
    }

    BigDecimal getNext() {
        if (direction == 1 && price.compareTo(maxPrice) > 0) {
            direction = -1;
        } else if (direction == -1 && price.compareTo(step) < 0) {
            direction = 1;
        }
        if (direction == 1) {
            price = price.add(gen());
        } else {
            price = price.subtract(gen());
        }
        return price.setScale(2, BigDecimal.ROUND_CEILING);
    }

    private BigDecimal gen() {
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        return randFromDouble.multiply(step);
    }
}
