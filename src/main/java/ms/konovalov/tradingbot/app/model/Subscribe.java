package ms.konovalov.tradingbot.app.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Subscribe {

    private static final String PREFIX = "trading.product.";

    private final String[] subscribeTo;

    private final String[] unsubscribeFrom;

    public static Subscribe subscribe(String id){
        return new Subscribe(new String[]{PREFIX + id}, null);
    }

    public static Subscribe unsubscribe(String id){
        return new Subscribe(null, new String[]{PREFIX + id});
    }

}
