package ms.konovalov.tradingbot.app;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class BuySellClientTest {

    private final String emuUrl = System.getProperty("emu.url") != null ? System.getProperty("emu.url") : "http://localhost:8080";
    private BuySellClient client = new BuySellClient(emuUrl);

    @Test
    public void testBuy() throws BuySellClient.BuySellException {
        String productId = client.buy("productId", BigDecimal.valueOf(10));
        Assert.assertNotNull(productId);
    }

    @Test
    public void testSell() throws BuySellClient.BuySellException {
        BigDecimal amount = client.sell("positionId");
        Assert.assertNotNull(amount);
    }

}
