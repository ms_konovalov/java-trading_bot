Web-Socket Trading bot
======================

This application represents a trading bot that connects to WebSocket and receives Quote updates for product. 
When price for product reaches desireable value it buys some amount of product and continue to monitor the price change.
When price reaches set range to sell bot sells previously bougt product.  
Buy and Sell operations are done via REST API.

Implementation
--------------
 
This application is made with Java, Akka, Spring Boot. It uses Gradle for build process, Docker for integration tests.
Also special emulator server is prepared for integration tests.

Package
-------

To build solution, run unit-tests, integration tests and generate test coverage report (Jacoco):  

```bash
./gradlew clean test itest jacocoTestReport
```

Test coverage report can be found at `./build/jacocoHtml/index.html`

Integration tests will pack `emuserver` into Docker container (docker and docker-compose must be installed) and execute tests against it.

Run application
---------------

Application can be started with

```bash
./gradlew bootRun --console=plain -w
```

If you want to pass different URL's for server connection you should

```bash
./gradlew bootRun --console=plain -w -PappArgs="['-ws', 'http://localhost:8080/me/subscribe', '-api', 'http://localhost:8080']"
```

After starting bot will ask you all interesting questions about product to buy, prices and amount. Interaction can look like this:

```text
Hello, I'm trading bot.
I can buy something for you!

What do you want to buy? (Please enter product id and press <Enter>)
ss

How many BUX should I spend? (Please enter decimal number and press <Enter>)
30

What is the price to buy? (Please enter decimal number and press <Enter>)
30

What is the min price to sell? (Please enter decimal number and press <Enter>)
60

What is the max price to sell? (Please enter decimal number and press <Enter>)
90

Let's go!
Job is done!
Your profit is 1275.47 BUX
```

Happy botting!