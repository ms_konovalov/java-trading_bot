package ms.konovalov.tradingbot.app;

import org.junit.Test;

import java.io.IOException;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

public class WebSocketClientTest {

    private final String emuUrl = System.getProperty("emu.url") != null ? System.getProperty("emu.url") : "http://172.19.0.1:32784";
    private TraditionalWebSocketClient client = new TraditionalWebSocketClient(emuUrl + "/subscriptions/me");

    @Test
    public void test() throws InterruptedException, IOException {
        //noinspection unchecked
        Consumer<String> mock = mock(Consumer.class);
        client.connect(() -> {
                },
                ex -> {
                    throw new RuntimeException(ex);
                },
                mock);
        verify(mock, timeout(2000)).accept(anyString());
        client.sendMessage("{ \"subscribeTo\": [ \"trading.product.12345\" ] }");
        Thread.sleep(3000);
        verify(mock, atLeast(1)).accept(anyString());
        client.sendMessage("{ \"unsubscribeFrom\": [ \"trading.product.12345\" ] }");
        Thread.sleep(2000);
        reset(mock);
        verify(mock, never()).accept(anyString());
        client.disconnect();
        verify(mock, never()).accept(anyString());
    }
}
