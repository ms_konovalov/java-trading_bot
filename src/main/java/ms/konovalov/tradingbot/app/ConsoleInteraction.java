package ms.konovalov.tradingbot.app;

import ms.konovalov.tradingbot.app.model.Protocol;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Class implements user interaction over console
 */
@Component
public class ConsoleInteraction {

    /**
     * Print work result
     *
     * @param message finish message
     */
    void consoleFinish(Object message) {
        if (message instanceof Protocol.Finish) {
            Protocol.Finish finish = (Protocol.Finish) message;
            if (finish.isSuccess()) {
                System.out.println("Job is done!");
                System.out.println("Your profit is " + finish.getProfit() + " BUX");
            } else {
                System.out.println("Sorry, I faced some problems:");
                System.out.println("Error connecting to WebSocket");
            }
        }
    }

    /**
     * Ask input data from client
     *
     * @return gathered data
     */
    Protocol.InitMessage consoleInteraction() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello, I'm trading bot.");
        System.out.println("I can buy something for you!");
        System.out.println();
        String productId = consoleId(scanner);
        BigDecimal amount = consoleBigDecimal(scanner,
                "How many BUX should I spend? (Please enter decimal number and press <Enter>)");
        BigDecimal buyPrice = consoleBigDecimal(scanner,
                "What is the price to buy? (Please enter decimal number and press <Enter>)");
        BigDecimal minSellPrice = consoleBigDecimal(scanner,
                "What is the min price to sell? (Please enter decimal number and press <Enter>)");
        BigDecimal maxSellPrice = consoleBigDecimal(scanner,
                "What is the max price to sell? (Please enter decimal number and press <Enter>)");
        System.out.println("Let's go!");
        return new Protocol.InitMessage(productId, amount, buyPrice, minSellPrice, maxSellPrice);
    }

    private String consoleId(Scanner scanner) {
        System.out.println("What do you want to buy? (Please enter product id and press <Enter>)");
        String productId = scanner.nextLine();
        System.out.println();
        return productId;
    }

    private BigDecimal consoleBigDecimal(Scanner scanner, String message) {
        System.out.println(message);
        BigDecimal result = null;
        while (result == null) {
            result = readValue(scanner);
        }
        System.out.println();
        return result;
    }

    private BigDecimal readValue(Scanner scanner) {
        try {
            return scanner.nextBigDecimal();
        } catch (InputMismatchException e) {
            System.out.println();
            System.out.println("Seems you made a mistake. Please try again");
            System.out.println();
            scanner.next();
            return null;
        }
    }

}
