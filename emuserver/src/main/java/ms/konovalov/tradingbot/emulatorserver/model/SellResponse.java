package ms.konovalov.tradingbot.emulatorserver.model;

import lombok.Data;

@Data
public class SellResponse {

    private String id;
    private String positionId;
    private Money profitAndLoss;
    private Product product;
    private Money investingAmount;
    private Money price;
    private int leverage;
    private BuyDirection direction;
    private String type;
    private long dateCreated;
}
