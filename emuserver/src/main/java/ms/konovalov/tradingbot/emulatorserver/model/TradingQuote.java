package ms.konovalov.tradingbot.emulatorserver.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;

public class TradingQuote extends RootEntity {

    public static final String TYPE = "trading.quote";

    public TradingQuote(String productId, BigDecimal price) {
        super(TYPE, new TradingQuoteBody(productId, price));
    }

    @Getter
    @RequiredArgsConstructor
    private static class TradingQuoteBody implements Body {
        private final String securityId;
        private final BigDecimal currentPrice;
    }


}
