package ms.konovalov.tradingbot.app.model;

import lombok.Data;

@Data
public class Product {
    private String securityId;
    private String symbol;
    private String displayName;
}